import { useEffect, useState } from "react";
import { Row, Col, Input, Typography, Card, Badge, Button } from "antd";
import { Event, Offer } from "../types";
import { DashboardStore } from "../store/DashboardStore";
import logo from "../logo.svg";

const Dashboard = () => {
  const artist = DashboardStore.useState((s) => s.artist);
  const events = DashboardStore.useState((s) => s.events);
  const [favourites, setFavourites] = useState<Event[]>([]);
  const [selectedEvent, setSelectedEvent] = useState<Event>();
  const [isEventSectionVisible, setIsEventSectionVisible] =
    useState<boolean>(false);
  const getFavourites =
    JSON.parse(localStorage.getItem("favourites") || "0") || [];

  const onSearch = async (value: string) => {
    try {
      const response = await fetch(
        `https://rest.bandsintown.com/artists/${value}?app_id=123`
      );
      const response2 = await fetch(
        `https://rest.bandsintown.com/artists/${value}/events?app_id=123&date=upcoming`
      );
      const artist = await response.json();
      const events = await response2.json();
      DashboardStore.update((s) => {
        s.artist = artist;
        s.events = events;
      });
      setIsEventSectionVisible(false);
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    if (getFavourites !== 0) {
      setFavourites([...getFavourites]);
    }
  }, []);

  const selectEvent = (event: Event) => {
    setSelectedEvent(event);
    setIsEventSectionVisible(true);
  };

  const addToFavourites = (event: Event) => {
    if (!getFavourites.some((favourite: Event) => favourite.id === event.id)) {
      let array = favourites;
      array.push(event);
      setFavourites([...array]);
      localStorage.setItem("favourites", JSON.stringify(favourites));
    }
  };

  const removeFromFavourites = (favourite: Event) => {
    const newFavourites = favourites.filter(
      (item: Event) => item.id !== favourite.id
    );
    setFavourites(newFavourites);
    localStorage.setItem("favourites", JSON.stringify(newFavourites));
  };

  return (
    <div style={{ padding: "20px 20px 20px 20px" }}>
      <img src={logo} alt="Logo" width={200} height={50} />
      <Row gutter={[24, 24]}>
        <Col xs={24} sm={12} md={12} lg={8} xl={8}>
          <Input.Search placeholder="Artist" onSearch={onSearch} enterButton />
          {!!Object.keys(artist).length && (
            <>
              <div
                style={{
                  display: "flex",
                  margin: "20px 0 20px 0",
                  alignItems: "center",
                }}
              >
                <img src={artist.thumb_url} width={100} />
                <Typography.Title level={2} style={{ marginLeft: 20 }}>
                  {artist.name}
                </Typography.Title>
              </div>
              <Typography.Title level={3}>Upcoming events</Typography.Title>
              {!!events.length ? (
                events.map((event: Event) => {
                  return (
                    <Badge.Ribbon
                      text={new Date(event.datetime).toLocaleDateString(
                        "en-us",
                        {
                          year: "numeric",
                          month: "long",
                          day: "numeric",
                        }
                      )}
                      color="blue"
                      key={event.id}
                    >
                      <Card
                        style={{ marginTop: 20 }}
                        hoverable
                        onClick={() => selectEvent(event)}
                      >
                        <b>
                          {event.venue.name} - {event.venue.city},{" "}
                          {event.venue.country}
                        </b>
                      </Card>
                    </Badge.Ribbon>
                  );
                })
              ) : (
                <div>No upcoming events</div>
              )}
            </>
          )}
        </Col>
        <Col xs={24} sm={12} md={12} lg={8} xl={8}>
          {isEventSectionVisible && !!selectedEvent ? (
            <div>
              <Typography.Title level={3}>
                Selected event information
              </Typography.Title>
              <Card title="Event information">
                {new Date(selectedEvent.datetime).toLocaleDateString("en-us", {
                  year: "numeric",
                  month: "long",
                  day: "numeric",
                })}
              </Card>
              <Card title="Venue information" style={{ marginTop: 20 }}>
                <p>
                  {selectedEvent.venue.name} - {selectedEvent.venue.city}
                </p>
              </Card>
              <Card title="Special offers" style={{ marginTop: 20 }}>
                {selectedEvent.offers.map((offer: Offer) => {
                  return (
                    <Typography.Link href={offer.url} target="_blank">
                      {offer.type}
                    </Typography.Link>
                  );
                })}
              </Card>
              <Button
                type="primary"
                style={{ width: "100%", marginTop: 20 }}
                onClick={() => addToFavourites(selectedEvent)}
              >
                Add to favourites
              </Button>
            </div>
          ) : (
            <Card style={{ height: 500 }}></Card>
          )}
        </Col>
        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
          <Typography.Title level={3}>Favourites</Typography.Title>
          {!!favourites.length ? (
            favourites.map((favourite: Event) => (
              <Badge.Ribbon
                text={new Date(favourite.datetime).toLocaleDateString("en-us", {
                  year: "numeric",
                  month: "long",
                  day: "numeric",
                })}
                color="blue"
                key={favourite.id}
              >
                {" "}
                <Card style={{ marginTop: 20 }}>
                  <div>
                    {favourite.lineup[0]} | {favourite.venue.name} -{" "}
                    {favourite.venue.city}, {favourite.venue.country}
                  </div>
                  <div>
                    <Button onClick={() => removeFromFavourites(favourite)}>
                      Remove
                    </Button>
                  </div>
                </Card>
              </Badge.Ribbon>
            ))
          ) : (
            <div>No favourite events</div>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default Dashboard;
