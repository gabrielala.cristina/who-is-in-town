import { Store } from "pullstate";
import { Artist, Event } from "../types";

export const DashboardStore = new Store({
  artist: {} as Artist,
  events: [] as Event[],
  favourites: [] as Event[],
});
