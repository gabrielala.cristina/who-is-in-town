import { Suspense } from 'react';
import Dashboard from './components/Dashboard';

const App = () => {

  return (
    <>
      <Suspense><Dashboard/></Suspense>
    </>
  );
};

export default App;
